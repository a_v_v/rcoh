import styles from './styles';
import { GitHub } from '@/src/assets/svg-inline';

function HeaderSignIn() {
  return (
    <a className="git" aria-label="go to github" href="https://github.com/varbachakov/rcoh">
      add hook
      <GitHub />

      <style jsx>
        {styles}
      </style>
    </a>
  );
}

export default HeaderSignIn;

